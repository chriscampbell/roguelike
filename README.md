# Roguelike

A simple C# 2D roguelike game created in Unity. All art was created using Aesprite by my partner Jade.

A playable WebGL version of the game can be found here: [https://chriscampbell.gitlab.io/roguelike](https://chriscampbell.gitlab.io/roguelike)