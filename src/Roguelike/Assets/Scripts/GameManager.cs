﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public BoardManager boardScript;
    public int playerCouragePoints = 100;
    [HideInInspector] public bool playersTurn = true;
    public float levelStartDelay = 2f;
    public float turnDelay = .1f;

    private int level = 0;
    private List<Enemy> enemies;
    private bool enemiesMoving;
    private Text levelText;
    private GameObject levelImage;
    private bool doingSetup;

    void Awake()
    {
        // Setup the singleton of GameManager and also set it to DontDestroyOnLoad
        if (instance == null)
        {
            instance = this;
        }       
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        enemies = new List<Enemy>();

        // Find the boardScript component
        boardScript = gameObject.GetComponent<BoardManager>();
    }

    void OnEnable()
    {
        // Tell our 'OnSceneWasLoaded' function to start listening for a scene change as soon as the script is enabled.
        SceneManager.sceneLoaded += OnSceneWasLoaded;
    }

    void OnDisable()
    {
        // Tell our 'OnSceneWasLoaded' function to stop listening for a scene change as soon as this script is disabled. Remember
        // to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnSceneWasLoaded;
    }

    private void InitGame()
    {
        doingSetup = true;

        levelImage = GameObject.Find("LevelImage");
        levelText = GameObject.Find("LevelText").GetComponent<Text>();
        levelText.text = "Day " + level;
        levelImage.SetActive(true);

        Invoke("HideLevelImage", levelStartDelay);

        enemies.Clear();
        boardScript.SetupScene(level);
    }

    private void HideLevelImage()
    {
        levelImage.SetActive(false);
        doingSetup = false;
    }

    private void OnSceneWasLoaded (Scene scene, LoadSceneMode mode)
    {
        level++;

        InitGame();
    }

    public void GameOver()
    {
        levelText.text = "After " + level + " days,\n you ran out of courage.";
        levelImage.SetActive(true);
        enabled = false;
    }

    void Update()
    {
        if (doingSetup || playersTurn || enemiesMoving)
        {
            return;
        }

        StartCoroutine(MoveEnemies());
    }

    public void AddEnemyToList(Enemy script)
    {
        enemies.Add(script);
    }

    IEnumerator MoveEnemies()
    {
        enemiesMoving = true;
        yield return new WaitForSeconds(turnDelay);

        if (enemies.Count == 0)
        {
            yield return new WaitForSeconds(turnDelay);
        }

        for (int i = 0; i < enemies.Count; i++)
        {
            enemies[i].MoveEnemy();
            yield return new WaitForSeconds(enemies[i].moveTime);
        }

        playersTurn = true;
        enemiesMoving = false;
    }
}
