﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour
{
    [Serializable]
    public class Count
    {
        public int minimum;
        public int maximum;

        public Count (int min, int max)
        {
            minimum = min;
            maximum = max;
        }
    }

    public int columns = 8;
    public int rows = 8;
    public Count obstacleCount = new Count(5, 9);
    public Count drinkCount = new Count(1, 5);
    public GameObject exit;
    public GameObject[] floorTiles;
    public GameObject[] obstacleTiles;
    public GameObject[] drinkTiles;
    public GameObject[] enemyTiles;
    public GameObject topLeftCornerTile;
    public GameObject topRightCornerTile;
    public GameObject bottomLeftCornerTile;
    public GameObject bottomRightCornerTile;
    public GameObject[] topWallTiles;
    public GameObject[] rightWallTiles;
    public GameObject[] bottomWallTiles;
    public GameObject[] leftWallTiles;

    private Transform boardHolder;
    private List<Vector3> gridPositions = new List<Vector3>();

    void InitialiseList()
    {
        gridPositions.Clear();

        for (int x = 1; x < columns - 1; x++)
        {
            for (int y = 1; y < rows - 1; y++)
            {
                gridPositions.Add(new Vector3(x, y, 0f));
            }
        }
    }

    void BoardSetup()
    {
        boardHolder = new GameObject("Board").transform;

        /*
         * Board is generated from the bottom left, to the top right.
         * x = columns
         * y = rows
         * 
         * Example positions:
         * top left     = x -1, y 8
         * top right    = x 8, y 8
         * bottom left  = x -1, y -1 
         * bottom right = x 8 y -1
         */
        for (int x = -1; x < rows + 1; x++)
        {
            for (int y = -1; y < columns + 1; y++)
            {
                GameObject toInstantiate = floorTiles[Random.Range(0, floorTiles.Length)];

                // Top left corner tile
                if (x == -1 && y == rows)
                {
                    toInstantiate = topLeftCornerTile;
                }

                // Top right corner tile
                else if (x == columns && y == rows)
                {
                    toInstantiate = topRightCornerTile;
                }

                // Bottom left corner tile
                else if (x == -1 && y == -1)
                {
                    toInstantiate = bottomLeftCornerTile;
                }

                // Bottom right corner tile
                else if (x == columns && y == -1)
                {
                    toInstantiate = bottomRightCornerTile;
                }

                // Top wall tiles
                else if (y == rows)
                {
                    toInstantiate = topWallTiles[Random.Range(0, topWallTiles.Length)];
                }

                // Right wall tiles
                else if (x == columns)
                {
                    toInstantiate = rightWallTiles[Random.Range(0, rightWallTiles.Length)];
                }

                // Bottom wall tiles
                else if (y == -1)
                {
                    toInstantiate = bottomWallTiles[Random.Range(0, bottomWallTiles.Length)];
                }

                // Left wall tiles
                else if (x == -1)
                {
                    toInstantiate = leftWallTiles[Random.Range(0, leftWallTiles.Length)];
                }

                GameObject instance = Instantiate(toInstantiate, new Vector3 (x, y, 0f), Quaternion.identity) as GameObject;

                instance.transform.SetParent(boardHolder);
            }
        }
    }

    Vector3 RandomPosition()
    {
        int randomIndex = Random.Range(0, gridPositions.Count);
        Vector3 randomPosition = gridPositions[randomIndex];
        gridPositions.RemoveAt(randomIndex);

        return randomPosition;
    }

    void LayoutObjectAtRandom(GameObject[] tileArray, int minimum, int maximum)
    {
        int objectCount = Random.Range(minimum, maximum + 1);

        for (int i = 0; i < objectCount; i++)
        {
            Vector3 randomPosition = RandomPosition();
            GameObject tileChoice = tileArray[Random.Range(0, tileArray.Length)];
            Instantiate(tileChoice, randomPosition, Quaternion.identity);
        }
    }

    public void SetupScene(int level)
    {
        BoardSetup();
        InitialiseList();
        LayoutObjectAtRandom(obstacleTiles, obstacleCount.minimum, obstacleCount.maximum);
        LayoutObjectAtRandom(drinkTiles, drinkCount.minimum, drinkCount.maximum);
        int enemyCount = (int)Mathf.Log(level, 2f);
        LayoutObjectAtRandom(enemyTiles, enemyCount, enemyCount);
        Instantiate(exit, new Vector3(columns - 1, rows - 1, 0f), Quaternion.identity);
    }
}
