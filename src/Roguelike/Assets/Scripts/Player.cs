﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MovingObject
{
    public int obstacleDamage = 1;
    public int pointsPerSmallDrink = 10;
    public int pointsPerBigDrink = 20;
    public float restartLevelDelay = 1f;
    public Text courageText;

    private Animator animator;
    private SpriteRenderer spriteRenderer;
    private int courage;
    private bool facingRight = true;

    // Start is called before the first frame update
    protected override void Start()
    {
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        courage = GameManager.instance.playerCouragePoints;

        courageText.text = "Courage: " + courage;

        base.Start();
    }

    private void OnDisable()
    {
        GameManager.instance.playerCouragePoints = courage;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.playersTurn)
        {
            return;
        }

        int horizontal = 0;
        int vertical = 0;

        horizontal = (int)Input.GetAxisRaw("Horizontal");
        vertical = (int)Input.GetAxisRaw("Vertical");

        if (horizontal != 0)
        {
            vertical = 0;

            if (horizontal == -1)
            {
                facingRight = false;
            }
            else
            {
                facingRight = true;
            }

            if (facingRight)
            {
                spriteRenderer.flipX = false;
            }
            else
            {
                spriteRenderer.flipX = true;
            }
        }

        if (horizontal != 0 || vertical != 0)
        {
            AttemptMove<Obstacle>(horizontal, vertical);
        }
    }

    protected override void AttemptMove <T> (int xDir, int yDir)
    {
        courage--;

        courageText.text = "Courage: " + courage;

        base.AttemptMove<T>(xDir, yDir);

        CheckIfGameOver();

        GameManager.instance.playersTurn = false;
    }

    private void OnTriggerEnter2D (Collider2D other)
    {
        if (other.tag == "Exit")
        {
            Invoke("Restart", restartLevelDelay);
            enabled = false;
        }
        else if (other.tag == "Drink Small")
        {
            courage += pointsPerSmallDrink;
            courageText.text = "+" + pointsPerSmallDrink + " Courage: " + courage;
            other.gameObject.SetActive(false);
        }
        else if (other.tag == "Drink Big")
        {
            courage += pointsPerBigDrink;
            courageText.text = "+" + pointsPerBigDrink + " Courage: " + courage;
            other.gameObject.SetActive(false);
        }
    }

    protected override void OnCantMove <T> (T component)
    {
        Obstacle hitObstacle = component as Obstacle;
        hitObstacle.DamageObstacle(obstacleDamage);

        animator.SetTrigger("playerChop");
    }

    private void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoseCourage (int loss)
    {
        animator.SetTrigger("playerHit");
        courage -= loss;
        courageText.text = "-" + loss + " Courage: " + courage;
        CheckIfGameOver();
    }

    private void CheckIfGameOver()
    {
        if (courage <= 0)
        {
            GameManager.instance.GameOver();
        }
    }
}
