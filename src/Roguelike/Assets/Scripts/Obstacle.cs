﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public int hp = 4;

    private Animator animator;
    private BoxCollider2D boxCollider;

    // Start is called before the first frame update
    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        boxCollider = gameObject.GetComponent<BoxCollider2D>();
    }

    public void DamageObstacle (int loss)
    {
        hp -= loss;

        animator.SetBool("Damaged", true);

        if (hp <= 0)
        {
            animator.SetTrigger("Destroyed");
            boxCollider.enabled = false;
        }
    }
}
